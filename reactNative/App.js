
import Constants from 'expo-constants'
import {ImageBackground,Button, StyleSheet, Text, TouchableWithoutFeedback , Alert, View, TextInput  } from 'react-native';
import React, {Component ,  useState } from 'react';
import CheckBox from '@react-native-community/checkbox';
import SignInPage from './components/signIn_page'
import SignUpPage from './components/signUp_page'




export default function App () {
  return (
    <ItemViewComponent/>
     
  );
}

class ItemViewComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state  = {
        Pressed : true,
    }
  }
     

  render() {
    const image = { uri: "https://mariongrandvincent.github.io/HTML-Personal-website/img-codePen/bg.jpg" };
   
     return <ImageBackground style={styles.container} source={image} >
        
    <View style={styles.viewButtons}>
    <TouchableWithoutFeedback
             onPress={() => {this.setState({Pressed: true}) }}
             >
           <View style={styles.button}>
             <Text style={this.state.Pressed ?  styles.textStyleAFterPressed : styles.textStyle }>SIGN IN</Text>
           </View>
         </TouchableWithoutFeedback>
         <TouchableWithoutFeedback
            onPress={() => { this.setState({Pressed: false}) }}
             >
           <View >
             <Text style={this.state.Pressed ? styles.textStyle : styles.textStyleAFterPressed} >SIGN UP</Text>
           </View>
         </TouchableWithoutFeedback>
  
     </View>
       
     {this.state.Pressed ? <SignInPage/> : <SignUpPage/>}
     
 
       </ImageBackground>
  }  


}


const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  buttonContainer:{
    alignItems: "center",
    justifyContent: "center",
    width: '92%',
    marginLeft: '4%',
    height: 50,
    backgroundColor: '#1161ee',
    borderRadius: 30,
  },
  textStyle:{
    color:'white',
    borderBottomColor: '#1161ee',
    marginRight: '10%',
    fontSize: 20,
    fontWeight: "600",
    textAlign: 'center'
  },
  textStyleAFterPressed:{
    color:'white',
    borderBottomColor: '#1161ee',
    borderBottomWidth:1,
    marginRight: '10%',
    fontSize: 20,
    fontWeight: "600",
    textAlign: 'center'
  },
  viewButtons: {
    marginTop: Constants.statusBarHeight * 3 ,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginLeft: '10%',
fontWeight: "900"
  },
  button: {
    color:'white'
  },
  input:{
    textDecorationColor: '#fff',
    width: '92%',
    marginLeft: '4%',
    padding: 20,
    color:'black',
    backgroundColor: '#fff',
    borderRadius: 30,
    opacity: 0.15,
  },
  label:{
    marginLeft: '10%',
    marginTop: 10,   
    color: '#fff',
  },
  mainContent:{
    marginTop: '15%'
  },
  horizontalLine:{
    marginTop: '27%' ,
    borderBottomWidth: 1,
    width: '92%',
    marginLeft: '4%',
    backgroundColor: '#fff',
    opacity: 0.15,
  },
  underHorizontalLine:{
    marginTop: '4%' ,
    fontSize: 17,
    textAlign: 'center',
   marginBottom:0,
    color: '#fff',
    opacity: 0.3,
  },
  checkBox:{
    color: 'white',
    marginTop: '5%' ,
    marginBottom: '5%' ,
    marginLeft: '10%',
  },
  checkBoxLabel:{
    marginTop: '6.5%' ,
    marginBottom: '5%' ,
    marginLeft: '10%',
    color: '#fff',
  },
});
