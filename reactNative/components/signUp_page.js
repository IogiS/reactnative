
import Constants from 'expo-constants'
import {ImageBackground,Button, StyleSheet, Text, TouchableOpacity , Alert, View, TextInput  } from 'react-native';
import React, {Component ,  useState } from 'react';
import CheckBox from '@react-native-community/checkbox';

export default function SignUpPage() {
  const image = { uri: "https://mariongrandvincent.github.io/HTML-Personal-website/img-codePen/bg.jpg" };
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  return (<View><View style={styles.mainContent}>
    <Text style={styles.label}>USERNAME</Text>
    <TextInput
        style={styles.input}
        placeholder="useless placeholder"
        keyboardType="default"
      />    
      <Text style={styles.label}>E-MAIL</Text>
    <TextInput
        style={styles.input}
        placeholder="useless placeholder"
        keyboardType="default"
      />    
      <Text style={styles.label}>PASSWORD</Text>
      <TextInput
        style={styles.input}
        placeholder="useless placeholder"
        keyboardType="default"
      />  
      <Text style={styles.label}>CONFIRM PASSWORD</Text>
    <TextInput
        style={styles.input}
        placeholder="useless placeholder"
        keyboardType="default"
      />       
       </View>
       <View style={{ flexDirection: 'row' }}>
       <CheckBox style={styles.checkBox}
       tintColors={ { true: '#1161ee', false: 'white'} } 
    disabled={false}
    value={toggleCheckBox}
    onValueChange={(newValue) => setToggleCheckBox(newValue)}
  />
  <Text style={styles.checkBoxLabel}>I agree</Text>
  
  </View>
  <View style={styles.buttonContainer}>
          <TouchableOpacity >
            <Text style={styles.buttonText}>SIGN UP</Text>
          </TouchableOpacity>
  </View>


</View>
     
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  buttonContainer:{
    alignItems: "center",
    justifyContent: "center",
    width: '92%',
    marginLeft: '4%',
    height: 50,
    backgroundColor: '#1161ee',
    borderRadius: 30,
  },
buttonText: {
    color: "white"
},
  textStyle:{
    color:'white',
    borderBottomColor: '#1161ee',
    borderBottomWidth: 1,
    marginRight: '10%',
  },
  viewButtons: {
    marginTop: Constants.statusBarHeight * 3 ,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginLeft: '10%',

  },
  button: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    color:'white'
  },
  input:{
    textDecorationColor: '#fff',
    width: '92%',
    marginLeft: '4%',
    padding: 15,
    color:'black',
    backgroundColor: '#fff',
    borderRadius: 30,
    opacity: 0.15,
  },
  label:{
    marginLeft: '10%',
    marginTop: 10,   
    color: '#fff',
  },
  mainContent:{
    marginTop: '15%'
  },
  horizontalLine:{
    marginTop: '27%' ,
    borderBottomWidth: 1,
    width: '92%',
    marginLeft: '4%',
    backgroundColor: '#fff',
    opacity: 0.15,
  },
  underHorizontalLine:{
    marginTop: '4%' ,
    fontSize: 17,
    textAlign: 'center',
   marginBottom:0,
    color: '#fff',
    opacity: 0.3,
  },
  checkBox:{
    color: 'white',
    marginTop: '5%' ,
    marginBottom: '5%' ,
    marginLeft: '10%',
  },
  checkBoxLabel:{
    marginTop: '6.5%' ,
    marginBottom: '5%' ,
    marginLeft: '10%',
    color: '#fff',
  },
});
