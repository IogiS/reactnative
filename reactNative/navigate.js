import React, {Component ,  useState } from 'react';
import SignInPage from './components/signIn_page'
import SignUpPage from './components/signUp_page'
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

const Stack = createStackNavigator();

export default function Navigate(){
    return <NavigationContainer>
            <Stack.Navigator>

                <Stack.Screen name='SignInPage' component={SignInPage} >
                    
                </Stack.Screen>

                <Stack.Screen name='SignUpPage' component={SignUpPage} >
                    
                    </Stack.Screen>
            </Stack.Navigator>
    </NavigationContainer>
}